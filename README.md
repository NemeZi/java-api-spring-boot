Instructions for Java API.

This Java API is made using Spring Boot and it uses names.json file which contains example of names of employees and amounts how many people share the same name in the company.

Instructions for using the API:

1. Run DevAcademyApplication.
2. Copy generated password from console.
3. Go to localhost:8080 address in your browser. Username will be ”user” and use the copied password.

Following addresses are available for localhost:8080/names

/all (shows all names and amounts)
/popular (sorts names by popularity)
/alphabetical (sorts names in alphabetical order)
/total (shows total amount of names and name amounts)
/search?name=Ville (Write name to see amount from only one person. You can change name, but if the name doesn’t exist you will receive an error)

Type one of the following addresses after /names

Made by: Tomi Heikkonen
