package com.heikkonen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class DevAcademyApplication {

    public static void main(String[] args) {

        SpringApplication.run(DevAcademyApplication.class, args);
    }
}
