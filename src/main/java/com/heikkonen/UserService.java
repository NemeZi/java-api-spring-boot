package com.heikkonen;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {

    //Creates a new list which will store the names from names.json file
    //Parses JSON file to Java so it can be stored into the users list
    public List<User> users;
    ObjectMapper mapper = new ObjectMapper();
    TypeReference<UserList> typeReference = new TypeReference<UserList>() {
    };
    InputStream inputStream = TypeReference.class.getResourceAsStream("/json/names.json");

    //Adds names to the users list
    public UserService() {

        try {
            UserList names = mapper.readValue(inputStream, typeReference);
            this.users = names.names;
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    //Shows all the names from the list
    public List<User> getNames() {
        return this.users;
    }

    //Sorts names from the list to the order by popularity
    public List<User> getPopular() {
        List<User> getSortedList = this.users.stream()
                .sorted(Comparator.comparingInt(User::getAmount).reversed())
                .collect(Collectors.toList());
        return getSortedList;
    }

    //Sorts names in alphabetical order.
    public List<User> getAlphabeticalList() {
        List<User> getAlphabeticalList = this.users.stream()
                .sorted(Comparator.comparing(User::getName))
                .collect(Collectors.toList());
        return getAlphabeticalList;
    }

    //Method which goes through the list and adds +1 to totalNames to keep count of the total names in the list
    //totalAmount keeps count of the total amounts how many people are sharing the same name the are in total
    public TotalResponse getTotalNameAmounts() {
        TotalResponse totalResponse = new TotalResponse();
        for (User user : this.users) {
            totalResponse.totalAmount += user.amount;
            totalResponse.totalNames++;
        }
        return totalResponse;
    }

    //Goes through the list of names and makes sure does entered name exist in User list
    public User getUserByName(String name) {
        for (User user : this.users) {
            if (user.name.equals(name)) {
                return user;
            }
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Entity not found");
    }
}



