package com.heikkonen;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/names")
public class NamesController {

    private UserService userService;

    public NamesController(UserService userService) {
        this.userService = userService;
    }

    //Shows all names from names.json file
    @GetMapping("/all")
    public List<User> getNames() {
        return userService.getNames();
    }

    //Sorts name by popularity. Most popular first
    @GetMapping("/popular")
    public List<User> getPopular() {
        return userService.getPopular();
    }

    //Sorts name in alphabetical order
    @GetMapping("/alphabetical")
    public List<User> getAlphabetical() {
        return userService.getAlphabeticalList();
    }

    //Shows total amount of names and name amounts
    @GetMapping("/total")
    public TotalResponse getTotalNameCount() {
        return userService.getTotalNameAmounts();
    }

    //Searches one person by name and shows the amount
    @GetMapping("/search")
    public User getUserByName(@RequestParam String name) {
        return userService.getUserByName(name);
    }
}
