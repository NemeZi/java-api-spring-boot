package com.heikkonen;

import lombok.Data;

@Data
public class User {

    public String name;
    public int amount;

    public User() {}

    public User(String name, int amount) {
        this.name = name;
        this.amount = amount;
    }
}
