package com.heikkonen;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

@SpringBootTest
class DevAcademyApplicationTests {

	@Autowired
	private NamesController controller;

	@Test
	void TestNames() {
		assertThat(this.controller.getNames()).as("All users").isNotNull();
	}

	@Test
	void TestTotal(){
		TotalResponse response = this.controller.getTotalNameCount();
		assertThat(response.totalAmount).isEqualTo(211);
		assertThat(response.totalNames).isEqualTo(20);
	}

	@Test
	void TestAlphabeticalOrder(){
		List<User> alphabetical = this.controller.getAlphabetical();
		List<String> names = new ArrayList<>() {};
		List<Integer> amounts = new ArrayList<>(){};
		for(User user: alphabetical){
			names.add(user.name);
		}
		assertThat(names).isSorted();
	}

	@Test
	void TestPopular(){
		List<User> popular = this.controller.getPopular();
		List<Integer> amounts = new ArrayList<>(){};
		for(User user: popular){
			amounts.add(user.amount);
		}
		assertThat(amounts).isSortedAccordingTo(Comparator.reverseOrder());
	}

	@Test
	void TestUserByName(){
		User userVille = this.controller.getUserByName("Ville");
		assertThat(userVille.name).isEqualTo("Ville");
		try {
			this.controller.getUserByName("Kalle");
			//User Kalle should not be found. If found fail the test
			fail("User Kalle was found!");
		} catch(Exception e) {
		}
	}
}
